function choosePage(num) {
    showData.start = showData.page = num;
    showData.getPageCount()
}

function nextPage () {
    if (showData.start < showData.totalPages) {
        showData.start++
        showData.page++
    }
    showData.getPageCount()
}
function prevPage () {
    if (showData.start > 0) {
        showData.start--
        showData.page--
    }
    showData.getPageCount()
}

var showData = {
    page: 0,
    start: 0,
    pages: 1,
    totalPages: 0,


    getPageCount: function () {
        var self = this;
        $.ajax({
            url: 'http://localhost/php-scrap/api/get/page_num.php',
            dataType: 'json',
            success: function (json) {
                self.totalPages = json.page_nums;
                $('#nav').empty();
                for (var i = 0; i < self.totalPages; i++) {
                    $('#nav').append('<li onclick="choosePage(' + i + ')">' + (i + 1) + '</li>')
                }
                $('#nav li:eq('+self.start+')').css({backgroundColor:'rgba(0,0,255,0.2)'});
                var maxShow = (self.start >= 9 ? self.start + 5 : 9);
                var lessShow = (self.start >= 9 ? self.start - 4 : 0);
                $('ul li:gt(' + maxShow + ')').hide();
                $('ul li:lt(' + lessShow + ')').hide();
                self.getData()
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.error(xhr.responseText);
            }
        })
    },
    getData: function () {
        var params = 'start=' + this.start + '&' + 'page=' + this.pages;
        var self = this;
        $.ajax({
            url: 'http://localhost/php-scrap/api/get/pages.php' + '?' + params,
            dataType: 'json',
            success: function (json) {
                $('#title').empty();
                $('#author').empty();
                $('#good').empty();
                $('#picture').empty();
                $('#contents').empty();
                $('#title').append(json[self.page].title);
                $('#author').append(json[self.page].author);
                $('#good').append('<span>按讚次數: '+json[self.page].good+'</span>');
                var images = json[self.page].images
                images.forEach(function (el) {
                    $('#picture').append(el);
                });
                var contents = json[self.page].contents
                contents.forEach(function (el) {
                    $('#contents').append(el);
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.error(xhr.responseText);
            }
        })
    }
}

$(document).ready(showData.getPageCount())   