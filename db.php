<?php

//dbconnect
class DbConn{
   private $pdo;
   private $dsn = "mysql:host=";
   private $host = 'localhost';
   private $user = 'root';
   private $pwd  = 'root';


   public function __construct()
   {
        $this->pdo = new PDO($this->dsn.$this->host,$this->user,$this->pwd);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        $this->pdo->exec("SET NAMES UTF8");
   }

   public function createDB($db_name)
   {
        $this->pdo->exec("CREATE DATABASE IF NOT EXISTS $db_name");
   }

   public function useDB($db_name)
   {
        $this->pdo->exec("use $db_name");
   }

   public function dropTable($table_name)
   {
        $this->pdo->exec("DROP TABLE IF EXISTS $table_name");
   }

   public function createTable($table_name,$columns)
   {
        $sql="CREATE TABLE IF NOT EXISTS $table_name(";
        $column='';

        foreach($columns as $key => $value){
        $column .= $key.' '.$value.',' ;
        }

        $sql.=$column.')';
        $sql = str_replace(',)',')',$sql);
        $this->pdo->exec($sql);
   }

   public function insertData($table,$columns,$values)
   {
        if(count($columns) != count($values)) throw new Exception('wrong params');
        $strColumns = implode(',',$columns);
        $q_ary = array();
    
        for($i=0;$i<count($values);$i++){
            $q_ary[]='?';
        }
        $q_str = implode(',',$q_ary);
    
        $sch = $this->pdo->prepare("INSERT INTO $table ($strColumns) VALUES ($q_str)");    
        $sch->execute( $values );
   }

   //query datas
   function queryData($column,$table,$is_print,$link)
   {
        if($link){
            $sql = "SELECT $column FROM $table WHERE link = ?";
            $sch = $this->pdo->prepare($sql);
            $sch->execute(array($link));
        }
        else{
            $sql = "SELECT $column FROM $table";
            $sch = $this->pdo->prepare($sql);
            $sch->execute();
        }
       
        $results = $sch->fetchAll();
        if($is_print){
            foreach($results as $result){
                print $result[$column]."\t"; 
             }
        }
        return $results;
   }

}

