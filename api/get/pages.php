<?php
ini_get('display_errors')?'':ini_set('display_errors',1);

$dir = dirname(__DIR__);
$dir = str_replace('/api','',$dir);
include $dir."/db.php";
include $dir.'/functions.php';

$get_string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING']:'';
parse_str($get_string, $get_array);
$page = isset($get_array['page'])?$get_array['page']:1;
$start = isset($get_array['start'])?$get_array['start']:0;

getPage($start,$page);


