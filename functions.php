<?php
require __DIR__ . '/vendor/autoload.php';
use JonnyW\PhantomJs\Client;

function getHtml($link,$bool)
{
    // echo $link;
    $ch = curl_init();
    $options = array(CURLOPT_URL => $link,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0', 
                CURLOPT_HEADER => true,
                CURLOPT_REFERER => 'https://www.google.com',
                CURLOPT_HTTPHEADER => array(
                    'Cookie: _ga=GA1.3.1532022604.1541488134; _gid=GA1.3.685439692.1541488134; __atuvc=20%7C45; has_js=1',
                ),
                CURLOPT_COOKIESESSION => true,
                CURLOPT_SSL_VERIFYPEER => false
               );
    curl_setopt_array($ch,$options);
   
    $html = curl_exec($ch);
    curl_close($ch);
    if($bool){
        return $html;
    }
    $dom = new DOMDocument();
    libxml_use_internal_errors(true);
    if($html) $dom->loadHtml( (string)$html );
    libxml_use_internal_errors(false);
    $xpath = new DOMXpath($dom);
    return $xpath;
}

//get Phantom response
function response($url){
    $client = Client::getInstance();
    $client->getEngine()->setPath(__DIR__.'/bin/phantomjs');
    $request  = $client->getMessageFactory()->createRequest();
    $response = $client->getMessageFactory()->createResponse();
    $request->setMethod('GET');
    $request->setUrl($url);
    $client->send($request, $response);
    return $response;
}

//use phantomjs
function scrapUsePhantom($url){
    // echo $url;
    $response = response($url);
    if($response->getStatus() === 200) {
        $html=$response->getContent();
        // echo '<pre>'.htmlentities($html).'</pre>';
        $regex_good = '/https:\/\/www\.facebook\.com\/plugins\/like\.php[^\"]*\"/';
        if(preg_match_all($regex_good,$html,$res)){
            echo ".";
            $p = $res[0][1];
            $link = str_replace('amp;','',$p);
            $link = str_replace('"','',$link);
            $response=response($link);
            if($response->getStatus() === 200) { 
                $html=$response->getContent();
                $regex_good = '/<span class=\"_5n6h _2pih\" id=\"u_0_3\">([0-9]+)<\/span>/';
                if(preg_match($regex_good,$html,$res)){
                    try{
                        $pdo= new DbConn;
                        $pdo->createDB('forTest');
                        $pdo->useDB('forTest');
                        $pdo->insertData('good',array('link','good'),array($url,$res[1]));
                    }catch(PDOExecption $e){
                        echo $e;
                    }   
                }
                          
            }
            else{
                echo '*'.$response->getStatus();
                sleep(1);
                scrapUsePhantom($url);
            }
        }
        else{
            sleep(1);
            scrapUsePhantom($url);
        }
    }else{
        echo '*'.$response->getStatus();
        sleep(1);
        scrapUsePhantom($url);
    }
}


//main scraper
function scraper($iniLink,$iniPageNum,$init){
  
    echo $iniPageNum;
  
    global $titles;
    $link = $iniLink;
    $pageNum = $iniPageNum;
    $results = getHtml($link,false)->query('//a');
    $regex = "/^\/news\/[0-9]+$/";
    $paths =array();
  
    if ($results->length > 0) {
        foreach($results as $result){
        //   echo '<pre>',$result->attributes->item(0)->value,'</pre>';
          $link = $result->attributes->item(0)->value;
          if(preg_match($regex,$link,$res)){
            // echo ($res[0]);
            if(!in_array($res[0],$paths)){
                $paths[]=$res[0];
            }
          }
        };
      
      for($i=0;$i<count($paths);$i++){
        //  for($i=0;$i<1;$i++){
         sleep(rand(1,2));
          $link = 'https://www.ithome.com.tw'.$paths[$i];
          // $link = 'https://www.ithome.com.tw/news/126860';
          $html = getHtml($link,true);
          // echo '<pre>',htmlentities($html),'</pre>';
          $regex_title = '/<h1(\s*[^>]*)*>\s*[^>]+\s*<\/h1>/';
          $regex_author = '/<span>文\/(\s*.*){3}/';
          $regex_created = '/<span class=\"created\">.*<\/span>/';
          $regex_content = '/<p(\s*[^>]*)*>.*<\/p>/';
          $regex_image = '/<img(\s*[^>]*)*>/';
  
          $title = '';
          $author = '';
          $images = array();
          $contents = array();
  
          if(preg_match($regex_title,$html,$res)){
              // echo $res[0];
              $title=$res[0];
          }
          if(preg_match($regex_author,$html,$res)){
              // $p=$res[0];
              if(preg_match_all('/<a(\s*[^>]*)*>/',$res[0],$res2)){
                  // echo($res2[0][0]);
                  foreach($res2[0] as $resATag){
                      $p = str_replace($resATag,'',$res[0]);
                  }
              }
              // echo $p;
              $author = $p;
          }
          if(preg_match_all($regex_image,$html,$res)){
              foreach($res[0] as $img){
                  if( !preg_match ('/width="180"|img_link/',$img) && !preg_match ('/width="300"|width\:\s*300/',$img) && !preg_match ('/ironman9thsidebar.png/',$img) ){
                      $images[] = $img;
                          // print_r ($img);
                  }
              }
          }
          if(preg_match_all($regex_content,$html,$res)){
              // print_r($res[0]);
              foreach($res[0] as $p){
                  if( !preg_match ('/<img/',$p) && !preg_match('/iThome Security/',$p) && !preg_match ('/[0-9]{4}-[0-9]{2}-[0-9]{2}/',$p) ){
                      if(preg_match_all('/<a(\s*[^>]*)*>/',$p,$res)){                    
                          foreach ($res[0] as $res){
                              $p = str_replace($res,'',$p);
                          }                    
                      }
                      $contents[] = $p;
                      // print_r ($p);
                  }
              }
          }
  
          try{
              $pdo= new DbConn;
              $pdo->createDB('forTest');
              $pdo->useDB('forTest');
  
              if($init){
                  $pdo->dropTable('title');
                  $pdo->dropTable('author');
                  $pdo->dropTable('good');
                  $pdo->dropTable('images');
                  $pdo->dropTable('contents');
                  $init=false;
              }
            
              $pdo->createTable('title',
                  array('id'       => 'INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY',
                        'link'     => 'VARCHAR(255) NOT NULL',
                        'title'    => 'VARCHAR(255) NOT NULL',
                        'reg_date' => 'TIMESTAMP'
                  )
              );
              $pdo->createTable('author',
                  array('id'       => 'INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY',
                        'link'     => 'VARCHAR(255) NOT NULL',
                        'author'    => 'VARCHAR(255) NOT NULL',
                        'reg_date' => 'TIMESTAMP'
                  )
              );
              $pdo->createTable('good',
                  array('id'       => 'INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY',
                        'link'     => 'VARCHAR(255) NOT NULL',
                        'good'    => 'LONGTEXT',
                        'reg_date' => 'TIMESTAMP'
                  )
              );
              $pdo->createTable('images',
                  array('id'       => 'INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY',
                        'link'     => 'VARCHAR(255) NOT NULL',
                        'images'   => 'LONGTEXT',
                        'reg_date' => 'TIMESTAMP'
                  )
              );
              $pdo->createTable('contents',
                  array('id'       => 'INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY',
                        'link'     => 'VARCHAR(255) NOT NULL',
                        'content'  => 'LONGTEXT',
                        'reg_date' => 'TIMESTAMP'
                  )
              );
  
              if(!in_array($title,$titles)){
                    $titles[]=$title;
                    $pdo->insertData('title',array('link','title'),array($link,$title));
                    $pdo->insertData('author',array('link','author'),array($link,$author));
                if(count($images)>0){
                    foreach($images as $image){
                        $pdo->insertData('images',array('link','images'),array($link,$image));
                    }
                }
                if(count($contents)>0){
                    foreach($contents as $content){
                        $pdo->insertData('contents',array('link','content'),array($link,$content));
                    }
                }
              }
  
          }catch(PDOException $e){
              echo $e->getMessage();
          }
          scrapUsePhantom($link);
      }
      
      }
  // $pageNum = 5990;
  $page = getHtml($iniLink,true);
    $regex = "/下一頁/";
    if(preg_match($regex,$page) && $pageNum<10){
    //    if(preg_match($regex,$page) && $pageNum<1){
      $pageNum++;
      $link = "https://www.ithome.com.tw/news?page={$pageNum}";
      scraper($link,$pageNum,false);
    }
    else{
        echo 'finished';
    }
  }

//api
function getPage($num_start,$num)
{
    header('Content-Type: application/json; charset=utf-8');
   
    $i = $num_start>0?$num_start:0;
    $page = $num>0?$num:1;
    $count = $i + $page;
    if($i>$count)
    {
        echo json_encode(array('error'=>'invald parameters'));
        return;
    }
    $result=array();
    
    try{
        $pdo = new DbConn;
        $pdo->useDB('forTest');
        $links = $pdo->queryData('link','title',false,false);
        // print_r($links[0]['link']);
        $count = ( $count <= count($links) ) ? $count : count($links);
        for(;$i < $count ; $i++){
            $titles = $pdo->queryData('title','title',false,$links[$i]['link']);
            $authors = $pdo->queryData('author','author',false,$links[$i]['link']);
            $goods = $pdo->queryData('good','good',false,$links[$i]['link']);
            $images = $pdo->queryData('images','images',false,$links[$i]['link']);
            $contents = $pdo->queryData('content','contents',false,$links[$i]['link']);
        
            foreach($titles as $title){
                // print $title['title']."\t";
                $result[$i]['title'] = $title['title'];
             }
            foreach($authors as $author){
                // print $title['title']."\t";
                $result[$i]['author'] = $author['author'];
            }
            foreach($goods as $good){
                // print $title['title']."\t";
                $result[$i]['good'] = $good['good'];
            }
            foreach($images as $image){
                // print $image['images']."\t";
                $result[$i]['images'][] = $image['images'];
             }
            foreach($contents as $content){
                // print $content['content']."\t";
                $result[$i]['contents'][] = $content['content'];
             }
        }
        $json = json_encode($result);
       
        echo $json;
    }catch(Exception $e){
        echo $e;
    }
}

//api
function getPageNums()
{
    header('Content-Type: application/json; charset=utf-8');
    try{
        $pdo = new DbConn;
        $pdo->useDB('forTest');
        $links = $pdo->queryData('link','title',false,false);
        $result = array('page_nums'=>count($links));
        echo json_encode($result);
    }catch(Exception $e){
        echo $e;
    }
}

